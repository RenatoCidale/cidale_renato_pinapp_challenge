import requests
from features.lib.apis.base_api import BaseApi
from features.lib.apis.response_api import ResponseApi


class User(BaseApi):

    def __init__(self, host):
        super().__init__(host)

    def post_new_pet(self,name):
        url = f"https://{self.host}/pet"
        headers = {
            'Content-Type': 'Application/json'
        }
        payload = {

            "id": 7,
            "category": {
                "id": 1,
                "name": "PERROS"
            },
            "name": name,
            "photoUrls": [
                "string"
            ],
            "tags": [
                {
                    "id": 0,
                    "name": "TAG"
                }
            ],
            "status": "available"

        }
        response = requests.post(url, headers=headers, json=payload)
        response_to_return = ResponseApi(response)
        return response_to_return

    def get_pet(self, id):
        url = f"https://{self.host}/pet/{id}"
        headers = {
            'Content-Type': 'Application/json'
        }
        response = requests.get(url, headers=headers)
        response_to_return = ResponseApi(response)
        return response_to_return

    def mod_pet1(self, id,name):
        url = f"https://{self.host}/pet"
        headers = {
            'Content-Type': 'Application/json'
        }
        payload = {

            "id": id,
            "category": {
                "id": 1,
                "name": "PERROS"
            },
            "name": name,
            "photoUrls": [
                "string"
            ],
            "tags": [
                {
                    "id": 0,
                    "name": "TAG"
                }
            ],
            "status": "sold"

        }
        response = requests.post(url, headers=headers, json=payload)
        response_to_return = ResponseApi(response)
        return response_to_return

    def delet_pet(self, id):
        url = f"https://{self.host}/pet/{id}"
        headers = {
            'Content-Type': 'Application/json'
        }
        response = requests.delete(url, headers=headers)
        response_to_return = ResponseApi(response)
        return response_to_return
