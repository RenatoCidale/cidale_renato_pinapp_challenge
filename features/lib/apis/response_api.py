import json

class ResponseApi:
    def __init__(self, response):
        self.status_code = None
        self.body = None
        if response != None:
            self.body = json.loads(response.text)
            self.status_code = response.status_code
