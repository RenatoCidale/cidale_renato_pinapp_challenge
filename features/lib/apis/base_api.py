class BaseApi:
    def __init__(self, host_param, token=None):
        self.host = host_param
        self.token = token if token != None else None
