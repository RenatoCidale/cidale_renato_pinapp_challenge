import json


class Utils:

    def __init__(self):
        pass

    def load_environment(self, file):
        with open("../environments/{}".format(file)) as f:
            data = json.load(f)
        return data

