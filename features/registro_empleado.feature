# Created by Renato at 30/08/2022
Feature: Validar funcionalidad grilla

  @TC1
  Scenario: Validacion de Grilla
    Given Ingresamos a la pagina web
    When el usuario hace click en la card elements
    Then el usuario selecciona la opcion web tables
    And el usuario selecciona el boton Add
    Then el usuario selecciona el boton Submit
    And el usuario visualiza los campos obligatorios para el registo
    And el usuario ingresa el nombre "Renato"
    And el usuario ingresa el apellido "Cidale"
    And el usuario ingresa el email "cidalerenato@gmail.com"
    And el usuario ingresa la edad "33"
    And el usuario ingresa el salario "100"
    And el usuario ingresa el departamento "IT - QA Automation"
    Then el usuario selecciona el boton Submit
    When el usuario busca el empleado registrado por el campo departamento "IT - QA Automation"
    Then el sistema muestra la busqueda correctamente