from features.pages.base_page import BasePage
from time import sleep


class WebTables(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    # Selectores (utilizo distintos selectores de elementos pero una buena practica seria utilizar ID's o en caso de no tenerlos
    # utilizar xpath relativos. A modo de ejemplo deje las tres opciones)

    btn_add = "//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/button[1]"
    txt_nombre = "//body[1]/div[4]/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/input[1]"
    txt_apellido = "//body[1]/div[4]/div[1]/div[1]/div[2]/form[1]/div[2]/div[2]/input[1]"
    txt_email = "userEmail"
    txt_edad = "age"
    txt_salario = "salary"
    txt_departamento = "department"
    btn_submit = "submit"
    txt_buscar = "//body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[1]/input[1]"
    txt_nombre_en_tabla = "//*[@class='rt-td']"

    def click_btn_add(self):
        self.click_on_element(self.btn_add, "xpath")
        sleep(2)

    def set_nombre_empleado(self, value):
        self.send_text(value, self.txt_nombre, "xpath")
        sleep(1)

    def set_apellido_empleado(self, value):
        self.send_text(value, self.txt_apellido, "xpath")
        sleep(1)

    def set_email_empleado(self, value):
        self.send_text(value, self.txt_email, "id")
        sleep(1)

    def set_edad_empleado(self, value):
        self.send_text(value, self.txt_edad, "id")
        sleep(1)

    def set_salario_empleado(self, value):
        self.send_text(value, self.txt_salario, "id")
        sleep(1)

    def set_departamento_empleado(self, value):
        self.send_text(value, self.txt_departamento, "id")
        sleep(1)

    def click_submit(self):
        self.click_on_element(self.btn_submit, "id")
        sleep(3)

    def buscar_empleado(self, value):
        self.send_text(value, self.txt_buscar, "xpath")
        sleep(2)

    def verificar_nombre_vacio(self):
        return self.get_text(self.txt_nombre, "xpath")

    def verificar_apellido_vacio(self):
        return self.get_text(self.txt_apellido, "xpath")

    def verificar_email_vacio(self):
        return self.get_text(self.txt_email, "id")

    def verificar_edad_vacio(self):
        return self.get_text(self.txt_edad, "id")

    def verificar_salario_vacio(self):
        return self.get_text(self.txt_salario, "id")

    def verificar_departamento_vacio(self):
        return self.get_text(self.txt_departamento, "id")

    def get_nombre_en_tabla(self):
        return self.get_text(self.txt_nombre_en_tabla, "xpath")
