from selenium.common.exceptions import ElementNotVisibleException, NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.keys import Keys


class BasePage:

    def __init__(self, driver):
        self.driver = driver

    def get_locator_type(self, locator_type):
        locator_type = locator_type.lower()
        if locator_type == "id":
            return By.ID
        elif locator_type == "name":
            return By.NAME
        elif locator_type == "class":
            return By.CLASS_NAME
        elif locator_type == "xpath":
            return By.XPATH
        elif locator_type == "css":
            return By.CSS_SELECTOR
        elif locator_type == "tag":
            return By.TAG_NAME
        elif locator_type == "link":
            return By.LINK_TEXT
        elif locator_type == "plink":
            return By.PARTIAL_LINK_TEXT

    def wait_for_element(self, locator_value, locator_type):
        web_element = None
        try:
            locator_type = locator_type.lower()
            locator_by_type = self.get_locator_type(locator_type)
            wait = WebDriverWait(self.driver, 25, poll_frequency=3,
                                 ignored_exceptions=[ElementNotVisibleException, NoSuchElementException])
            web_element = wait.until(ec.presence_of_element_located((locator_by_type, locator_value)))
        except:
            assert False
        return web_element

    def click_on_element(self, locator_value, locator_type):
        try:
            locator_type = locator_type.lower()
            web_element = self.wait_for_element(locator_value, locator_type)
            web_element.click()
        except:
            assert False

    def send_keys(self, locator_value, locator_type):
        locator_type = locator_type.lower()
        web_element = self.wait_for_element(locator_value, locator_type)
        web_element.send_keys(Keys.TAB)

    def send_text(self, text: object, locator_value: object, locator_type: object) -> object:
        try:
            locator_type = locator_type.lower()
            web_element = self.wait_for_element(locator_value, locator_type)
            web_element.clear()
            web_element.send_keys(text)
        except:
            assert False

    def get_text(self, locator_value, locator_type):
        element_text = None
        try:
            locator_type = locator_type.lower()
            web_element = self.wait_for_element(locator_value, locator_type)
            element_text = web_element.text
        except:
            assert False
        return element_text
