from features.pages.base_page import BasePage
from time import sleep


class MainPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    # Selectores

    card_element = "//body[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]"
    card_forms = "//body[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[3]"
    card_alerts = "//body[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[3]"
    card_widgets = "//body[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]"
    card_interactions = "//body[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[5]/div[1]/div[3]"

    def click_card_elements(self):
        self.click_on_element(self.card_element, "xpath")
        sleep(4)
