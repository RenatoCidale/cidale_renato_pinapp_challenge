from features.pages.base_page import BasePage
from time import sleep


class Elements(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    # Selectores

    opc_text_box = ""
    opc_check_box = ""
    opc_radioButton = ""
    opc_web_page = "//body[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[4]"
    opc_buttons = ""
    opc_links = ""
    opc_broken_links = ""
    opc_upload_dowload = ""

    def click_opc_webTable(self):
        self.click_on_element(self.opc_web_page, "xpath")
        sleep(2)
