from behave import *
from features.pages.elements_page import Elements


@then('el usuario selecciona la opcion web tables')
def step_imp(context):
    elements = Elements(context.driver)
    elements.click_opc_webTable()
