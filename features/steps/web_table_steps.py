from time import sleep
from behave import *
from features.pages.web_tables_page import WebTables


@then('el usuario selecciona el boton Add')
def step_imp(context):
    web_table = WebTables(context.driver)
    web_table.click_btn_add()


@then('el usuario ingresa el nombre "{value}"')
def step_imp(context, value):
    web_table = WebTables(context.driver)
    web_table.set_nombre_empleado(value)


@then('el usuario ingresa el apellido "{value}"')
def step_imp(context, value):
    web_table = WebTables(context.driver)
    web_table.set_apellido_empleado(value)


@then('el usuario ingresa el email "{value}"')
def step_imp(context, value):
    web_table = WebTables(context.driver)
    web_table.set_email_empleado(value)


@then('el usuario ingresa la edad "{value}"')
def step_imp(context, value):
    web_table = WebTables(context.driver)
    web_table.set_edad_empleado(value)


@then('el usuario ingresa el salario "{value}"')
def step_imp(context, value):
    web_table = WebTables(context.driver)
    web_table.set_salario_empleado(value)


@then('el usuario ingresa el departamento "{value}"')
def step_imp(context, value):
    web_table = WebTables(context.driver)
    web_table.set_departamento_empleado(value)


@then('el usuario selecciona el boton Submit')
def step_imp(context):
    web_table = WebTables(context.driver)
    web_table.click_submit()


@When('el usuario busca el empleado registrado por el campo departamento "{value}"')
def step_imp(context, value):
    web_table = WebTables(context.driver)
    web_table.buscar_empleado(value)
    sleep(7)


@Then('el usuario visualiza los campos obligatorios para el registo')
def step_imp(context):
    web_table = WebTables(context.driver)
    assert web_table.verificar_nombre_vacio() == "", "el campo nombre no se encuentra vacio"
    assert web_table.verificar_apellido_vacio() == "", "el campo apellido no se encuentra vacio"
    assert web_table.verificar_email_vacio() == "", "el campo email no se encuentra vacio"
    assert web_table.verificar_edad_vacio() == "", "el campo edad no se encuentra vacio"
    assert web_table.verificar_salario_vacio() == "", "el campo salario no se encuentra vacio"
    assert web_table.verificar_departamento_vacio() == "", "el campo departamento no se encuentra vacio"


@Then('el sistema muestra la busqueda correctamente')
def step_imp(context):
    web_table = WebTables(context.driver)
    assert web_table.get_nombre_en_tabla() == "Renato", "El nombre del empleado no se encuenta registrado"
