from time import sleep
from behave import *
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


@given("Ingresamos a la pagina web")
def step_imp(context):
    chrome_options = Options()
    context.driver = webdriver.Chrome(executable_path=".\\utils\\chromedriver\\chromedriver.exe")
    context.flag = True
    context.driver.maximize_window()
    context.driver.get(context.url_front)
    context.driver.execute_script("window.scrollTo(0, 250)")
    sleep(4)


@given('el usuario ingresa a la pagina web')
def step_imp(context):
    pass
