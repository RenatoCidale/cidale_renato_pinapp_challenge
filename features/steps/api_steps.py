from behave import *
from features.lib.apis.user_api import User


@then('el usuario completa el formulario de registro con el nombre {value}')
def step_imp(context, value):
    host = context.url_back
    user = User(host)
    response = user.post_new_pet(value)
    context.id = response.body['id']
    assert response.status_code == 200, f"El status code esperado es: {200}, y esta devolviendo {response.status_code}"


@then('se valida el registro de la mascota')
def step_imp(context):
    host = context.url_back
    user = User(host)
    response = user.get_pet(context.id)
    assert response.status_code == 200, f"El status code esperado es: {200}, y esta devolviendo {response.status_code}"


@then('se modifican los datos de la mascota con el nombre {value}')
def step_imp(context, value):
    host = context.url_back
    user = User(host)
    response = user.mod_pet1(context.id, value)
    response.name = response.body['name']
    assert response.status_code == 200, f"El status code esperado es: {200}, y esta devolviendo {response.status_code}"


@then('se valida la modificacion de la mascota con el nombre {value}')
def step_imp(context, value):
    host = context.url_back
    user = User(host)
    response = user.get_pet(context.id)
    context.status = response.body['status']
    assert response.body['name'] == value
    assert response.status_code == 200, f"El status code esperado es: {200}, y esta devolviendo {response.status_code}"


@then('se elimina el registro de la mascota')
def step_imp(context):
    host = context.url_back
    user = User(host)
    response = user.delet_pet(context.id)
    assert response.status_code == 200, f"El status code esperado es: {200}, y esta devolviendo {response.status_code}"


@then('se valida la eliminacion del registro')
def step_imp(context):
    host = context.url_back
    user = User(host)
    response = user.get_pet(context.id)
    assert response.body['message'] == "Pet not found"
    assert response.status_code == 404, f"El status code esperado es: {404}, y esta devolviendo {response.status_code}"
