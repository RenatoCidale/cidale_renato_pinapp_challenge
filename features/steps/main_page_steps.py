from time import sleep
from behave import *
from features.pages.main_page import MainPage


@when('el usuario hace click en la card elements')
def step_imp(context):
    main_page = MainPage(context.driver)
    main_page.click_card_elements()
    sleep(5)
