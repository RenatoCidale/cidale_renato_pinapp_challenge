import json


def before_all(context):
    context.values = {}
    context.actual_environment = context.config.userdata.get("environment")
    with open("../environments/{0}".format(context.actual_environment)) as json_file:
        context.environment_variables = json.load(json_file)
    context.url_front = context.environment_variables["url_front"]
    context.url_back = context.environment_variables["url_back"]
    context.flag = None


def after_scenario(context, scenario):
    if scenario.status != "passed" and context.flag == True:
        context.driver.close()
    else:
        pass
