# Created by Renato at 31/08/2022
Feature: Api_test

  @apitest
  Scenario Outline: Registro, modificacion y eliminacion de mascotas de un PetShop
    Given el usuario ingresa a la pagina web
    Then el usuario completa el formulario de registro con el nombre <nombre>
    And se valida el registro de la mascota
    Then se modifican los datos de la mascota con el nombre <nombremod>
    And se valida la modificacion de la mascota con el nombre <nombremod>
    Then se elimina el registro de la mascota
    Then se valida la eliminacion del registro

    Examples:
      | nombre | nombremod |
      | PIRLO  | RUFFO     |
      | BAKO   | SCHWENY   |
